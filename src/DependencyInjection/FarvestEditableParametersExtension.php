<?php
/** *****************************************************************************************************************
 *  FarvestEditableParameterExtension.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/09/13
 *  ***************************************************************************************************************** */

namespace Farvest\EditableParametersBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Exception;

/** *****************************************************************************************************************
 *  Class FarvestEditableParameterExtension
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EditableParameterBundle\DependencyInjection
 *  ***************************************************************************************************************** */
class FarvestEditableParametersExtension extends Extension
{
    /** *************************************************************************************************************
     *  @param array $configs
     *  @param ContainerBuilder $container
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}