# Farvest Editable Parameter Bundle

TODO

## Installation

Use composer manager to install it
```bash
composer install farvest/editable-parameter-bundle
```
To add the parameter entity to your database, you just have to make a standard migration.
```bash
php bin/console make:migration
php bin/console doctrine:migrations:migrate
```
## License

[MIT](https://choosealicence.com/licenses/mit/)
