<?php
/** *****************************************************************************************************************
 *  ParameterService.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/09/13
 *  ***************************************************************************************************************** */

namespace Farvest\EditableParametersBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Farvest\EditableParametersBundle\Entity\Parameter;
use Farvest\EditableParametersBundle\Service\Exception\EditableParameterNotFoundException;

/** *****************************************************************************************************************
 *  Class ParameterService
 *  -----------------------------------------------------------------------------------------------------------------
 *  Class to get the value of a parameter stored in the database that can be updates in admin mode
 *  -----------------------------------------------------------------------------------------------------------------
 *  @example    $parameter = new ParameterService();
 *              $parameter->get('mail.sender');
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EditableParametersBundle\Service
 *  ***************************************************************************************************************** */
class ParameterService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /** *************************************************************************************************************
     *  ParameterService constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param EntityManagerInterface $manager
     *  ************************************************************************************************************* */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /** *************************************************************************************************************
     *  Return the value of the parameter with its name. Return an exception if not found
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string|null $parameterName
     *  @return string|null
     *  @throws EditableParameterNotFoundException
     *  ************************************************************************************************************* */
    public function get(?string $parameterName)
    {
        /** @var Parameter|null $parameter */
        $parameter = $this->manager
            ->getRepository(Parameter::class)
            ->findOneBy(['parameterName' => $parameterName]);

        if ($parameter !== null) {
            return $parameter->getParameterValue();
        }

        throw new EditableParameterNotFoundException(
            sprintf('EditableParametersBundle error : Parameter with name \'%s\' doesn\'t exist in database.', $parameterName)
        );
    }

    public function loader(): array
    {
        $params = [];

        $parameters = $this->manager
            ->getRepository(Parameter::class)
            ->findAll();

        foreach($parameters as $parameter) {
            $params[$parameter->getParameterName()] = $parameter->getParameterValue();
        }

        return $params;
    }
}