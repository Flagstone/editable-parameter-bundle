<?php
/** *****************************************************************************************************************
 *  Parameter.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/09/12
 *  ***************************************************************************************************************** */

namespace Farvest\EditableParametersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Farvest\BaseEntityBundle\Entity\AbstractMappedSuperclass;

/** *****************************************************************************************************************
 *  Class Parameter
 *  -----------------------------------------------------------------------------------------------------------------
 *  @ORM\Entity(
 *      repositoryClass = "Farvest\EditableParametersBundle\Repository\ParameterRepository"
 *  )
 *  @ORM\Table(
 *      name            = "fv_editable_parameter",
 *      indexes         = {
 *          @ORM\Index(
 *              name        = "parameter_name_idx",
 *              columns     = {"parameter_name"}
 *          )
 *      }
 *  )
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EditabledEntityBundle\Entity
 *  ***************************************************************************************************************** */
class Parameter extends AbstractMappedSuperclass
{
    /*  =============================================================================================================
     *  Attributes
     *  ============================================================================================================= */

    /* ----- Mapped to database ------------------------------------------------------------------------------------- */

    /**
     *  @var string     The name of the parameter.
     *  -------------------------------------------------------------------------------------------------------------
     *  @ORM\Column(
     *      name        = "parameter_name",
     *      type        = "string",
     *      length      = 63,
     *      nullable    = false
     *  )
     */
    private $parameterName;

    /**
     *  @var string     The type of the parameter (eg. bool, string, integer...).
     *  -------------------------------------------------------------------------------------------------------------
     *  @ORM\Column(
     *      name        = "parameter_type",
     *      type        = "string",
     *      length      = 15,
     *      nullable    = false
     *  )
     */
    private $parameterType;

    /**
     *  @var string      The value of the parameter (store in string format for all parameter).
     *  --------------------------------------------------------------------------------------------------------------
     *  @ORM\Column(
     *      name        = "parameter_value",
     *      type        = "string",
     *      length      = 255
     *  )
     */
    private $parameterValue;

    /*  =============================================================================================================
     *  Class setters
     *  ============================================================================================================= */

    /**
     *  @param string|null $parameterName
     *  @return Parameter
     */
    public function setParameterName(?string $parameterName): self
    {
        $this->parameterName = $parameterName;
        return $this;
    }

    /**
     *  @param string|null $parameterType
     *  @return Parameter
     */
    public function setParameterType(?string $parameterType): self
    {
        $this->parameterType = $parameterType;
        return $this;
    }

    /**
     *  @param string|null $parameterValue
     *  @return Parameter
     */
    public function setParameterValue(?string $parameterValue): self
    {
        $this->parameterValue = $parameterValue;
        return $this;
    }

    /*  =============================================================================================================
     *  Class getters
     *  ============================================================================================================= */

    /**
     *  @return string|null
     */
    public function getParameterName(): ?string
    {
        return $this->parameterName;
    }

    /**
     *  @return string|null
     */
    public function getParameterType(): ?string
    {
        return $this->parameterType;
    }

    /**
     *  @return string|null
     */
    public function getParameterValue(): ?string
    {
        return $this->parameterValue;
    }
}
