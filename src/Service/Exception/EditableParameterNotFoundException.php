<?php
/** *****************************************************************************************************************
 *  EditableParameterNotFoundException.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/09/12
 *  ***************************************************************************************************************** */

namespace Farvest\EditableParametersBundle\Service\Exception;

use Exception;

/**
 * Class EditableParameterNotFoundException
 * @package Farvest\EditableParameterBundle\Service\Exception
 */
/** *****************************************************************************************************************
 *  Class EditableParameterNotFoundException
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EditableParametersBundle\Service\Exception
 *  ***************************************************************************************************************** */
class EditableParameterNotFoundException extends Exception
{

}