<?php
/** *****************************************************************************************************************
 *  CreateParameterCommand.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/09/16
 *  ***************************************************************************************************************** */

namespace Farvest\EditableParametersBundle\Command;

use Farvest\BaseEntityBundle\Service\SaverService;
use Farvest\EditableParametersBundle\Entity\Parameter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/** *****************************************************************************************************************
 *  Class CreateDefaultParameterCommand
 *  -----------------------------------------------------------------------------------------------------------------
 *  This class allow user or application to create editable parameter with console instruction
 *  Usage : php bin/console farvest-parameter:create-default [parameter_name] [parameter_type] [parameter_value]
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EditableParameterBundle\DependencyInjection
 *  ***************************************************************************************************************** */
class CreateDefaultParameterCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = "farvest-editable-parameters:create-default";

    /**
     * @var SaverService
     */
    private $saver;

    /** *************************************************************************************************************
     *  CreateParameterCommand constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param SaverService $saver
     *  ************************************************************************************************************* */
    public function __construct(SaverService $saver)
    {
        parent::__construct();
        $this->saver = $saver;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create a new parameter (if not exists)')
            ->setHelp('This command allow you to create a new editable parameter, if it doesn\'t exists')
            ->addArgument('name', InputArgument::REQUIRED)
            ->addArgument('type', InputArgument::REQUIRED)
            ->addArgument('value', InputArgument::REQUIRED);
    }

    /** *************************************************************************************************************
     *  @param InputInterface $input
     *  @param OutputInterface $output
     *  @return int|void|null
     *  ************************************************************************************************************* */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $type = $input->getArgument('type');
        $value = $input->getArgument('value');

        $parameter = new Parameter();

        $parameter
            ->setParameterName($name)
            ->setParameterType($type)
            ->setParameterValue($value);

        $result = $this->saver->saveIfNotExists($parameter, ['parameterName' => $name]);

        if ($result === true) {
            $output->writeln([
                '',
                'Create a new editable parameter : '.$name.', type : '.$type.' with value '.$value
            ]);
        } else {
            $output->writeln([
                '',
                'Parameter : '.$name.' already exists. Change it with the admin panel.'
            ]);
        }
    }
}
